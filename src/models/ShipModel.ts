import { Orientation } from '../components/Board/Ship/Ship'

export interface ShipModel {
  id: number
  length: number
  orientation: Orientation
  position: {
    x: number
    y: number
  },
  hit: number[]
}