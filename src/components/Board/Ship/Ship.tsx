import React  from 'react'
import { DragAndDropActionCreator, StartDragPayload } from '../../../reducer/dragAndDropReducer/DragAndDropActions'
import { ReducerContext } from '../../../reducer/appReducer/ReducerContext'
import { ShipModel } from '../../../models/ShipModel'
import { ShipsActionCreator } from '../../../reducer/shipsReducer/shipsActions'
import './Ship.scss'
import { validateShipPosition } from '../../../reducer/shipsReducer/shipsHelper'

const Vertical = "column"
const Horizontal = "row"
export type Orientation = typeof Horizontal | typeof Vertical

export const Ship: React.FC<{ shipId: number }> = ({shipId}) => {

  const [draggedCasiller, setDraggedCasiller] = React.useState<number>(0)
  const [state, dispatch] = React.useContext(ReducerContext)
  const { ships, gameState } = state
  const { playing, searching } = gameState
  const ship = ships.find( (ship: ShipModel) => ship.id === shipId)
  const { length, id, orientation, position, hit } = ship
  const { x, y } = position
  const isColumn = orientation === Vertical

  const handleDrag = (event: React.DragEvent<HTMLDivElement>) => {
    event.dataTransfer.setDragImage(document.createElement('a'), 0,0)
    dispatch(DragAndDropActionCreator.startDrag({
      shipLength: length,
      draggedFrom: draggedCasiller,
      orientation,
      id
    }))
  }

  const flip = (evt: any) => {
    evt.preventDefault()
    const shipInfo: StartDragPayload = { shipLength: length, draggedFrom: draggedCasiller, orientation: isColumn? Horizontal : Vertical, id }
    if (validateShipPosition(ships)(shipInfo)(position) && !(playing || searching))
      dispatch(ShipsActionCreator.changeOrientation({ id }))
  }

  return (
    <div
      className={'battleShipContiner'}
      draggable={!(playing || searching)}
      style={{
        flexDirection: orientation,
        top: y * 50,
        left: x * 50,
        height: isColumn? 50 * length - 2 * 5 : 40,
        width: isColumn? 40 : 50 * length - 2 * 5,
      }}
      onDragStart={handleDrag}
      onContextMenu={flip}>

      {Array.from(Array(length)).map((_,i) =>
        <div
          className={`battleShip ${ hit.some( (n: number) => i === n)? 'hit' : '' }`}
          onMouseEnter={() => setDraggedCasiller(i)}
          key={i}
          style={{
            marginTop: isColumn ? ( i === 0 ? 0 : 10 ) : 0,
            marginRight: !isColumn ? ( i === length - 1 ? 0 : 10 ) : 0,
          }}
        />
        )}

    </div>
  )
}