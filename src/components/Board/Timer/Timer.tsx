import React, { useEffect } from 'react'
import './Timer.scss'
import { ReducerContext } from '../../../reducer/appReducer/ReducerContext'
import { GameActionCreator } from '../../../reducer/gameReducer/gameActions'

interface Props {

}

export const Timer: React.FC<Props> = () => {
  const [state, dispatch] = React.useContext(ReducerContext)
  const { gameState } = state
  const { time, isTimerActive, playing } = gameState

  useEffect(() => {
    let interval: any = null;
    if (isTimerActive) {
      interval = setInterval(() => {
        if (time > 0) dispatch(GameActionCreator.takeFromTimer())
        else dispatch(GameActionCreator.pauseTimer())
      }, 1000);
    } else if (!isTimerActive && time !== 0) {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isTimerActive, time]);


  if (!playing) return null

  if (playing && !isTimerActive) {
    return <h3 className={'Timer'}> El enemigo esta jugando... 😟 </h3>
  }

  return (
    <div className="Timer">
        Es tu turno! tenes {time}s para jugar
    </div>
  );
}