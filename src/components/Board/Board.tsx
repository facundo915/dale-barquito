import React from 'react'
import './Board.scss'
import { Casiller } from './Casiller/Casiller'
import { ShootableCasiller } from './ShootableCasiller/ShootableCasiller'
import { ReducerContext } from '../../reducer/appReducer/ReducerContext'
import { Ship } from './Ship/Ship'
import { client, initialShips } from '../../constants/Constants'

interface BoardProps {
  rows: number,
  columns: number
  shootable?: boolean
}




export const Board: React.FC<BoardProps> = ({rows, columns, shootable, children}) => {

  const [ state ] = React.useContext(ReducerContext)
  const { gameState } = state
  const { playing } = gameState

  if (shootable) {
    if (!playing) return null
    return (
      <div className={'board-container'}>
        <div className={'board'}>
          {Array.from(Array(rows * columns).keys()).map( i =>
            <ShootableCasiller x={ i % rows } y={ Math.floor((i / columns))} key={i}/>
          )}
        </div>
      </div>
    )
  }

  return (
    <div className={'board-container'}>
      <div className={`board ${playing? "margin-right" : ""}`}>
          {Array.from(Array(rows * columns).keys()).map( i =>
            <Casiller x={ i % rows } y={ Math.floor((i / columns))} key={i}/>
          )}
      </div>
      {initialShips.map( ship => <Ship shipId={ship.id}/>)}
      {children}
    </div>
  )
}
