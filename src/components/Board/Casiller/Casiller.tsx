import React from 'react'
import './Casiller.scss'
import { ReducerContext } from '../../../reducer/appReducer/ReducerContext'
import { BoardReducerActionCreator } from '../../../reducer/boardReducer/BoardActions'
import { ShipsActionCreator } from '../../../reducer/shipsReducer/shipsActions'
import { validateShipPosition } from '../../../reducer/shipsReducer/shipsHelper'

interface CasillerProps {
  x: number
  y: number
}

export const Casiller: React.FC<CasillerProps> = ({x, y}) => {

  const [ isDragOver, setIsDragOver ] = React.useState(false)
  const [ state, dispatch ] = React.useContext(ReducerContext)
  const { boardState, dragAndDrop, ships } = state
  const { currentDrag } = dragAndDrop
  const { enemyShot, enemyHit } = boardState[`${x}${y}`] || { enemyShot: false, enemyHit: false }
  React.useEffect(() => {
    dispatch(BoardReducerActionCreator.initializeCasiller({x, y}))
  },[])

  const handleDragOver = (evt: any) => {
    evt.preventDefault()
    const { draggedFrom, orientation } = currentDrag
    const column = orientation === "column"
    const positionToValidate = { x: !column? x - draggedFrom : x, y: column? y - draggedFrom : y }

    if (!isDragOver && validateShipPosition(ships)(currentDrag)({ x: positionToValidate.x , y: positionToValidate.y })) {
      dispatch(ShipsActionCreator.moveShip({ id: currentDrag.id, x: positionToValidate.x , y: positionToValidate.y }))
      setIsDragOver(true)
    }
  }

  const handleDragLeave = () => {
    setIsDragOver(false)
  }

  const handleDrop = () => {}
  return (
    <div className={`casiller ${enemyShot? `shot ${ enemyHit ? 'hit' : ''}` : ''}`}
         onDragOver={handleDragOver}
         onDragLeave={handleDragLeave}
         onDrop={handleDrop}
    />
  )
}
