import React from 'react'
import { GameActionCreator } from '../../../reducer/gameReducer/gameActions'
import { client } from '../../../constants/Constants'
import { ReducerContext } from '../../../reducer/appReducer/ReducerContext'
import './PlayButton.scss'
import { mapShipToActualPositions } from '../../../reducer/shipsReducer/shipsHelper'

interface Props {

}

export const PlayButton: React.FC<Props> = () => {
  const [state, dispatch] = React.useContext(ReducerContext)

  const { gameState, ships } = state
  const { playing, searching } = gameState

  const mapShipsToPositionArray = () => {
    return ships.map(mapShipToActualPositions)
      .reduce( (acc: {x: number, y: number}[], next: {x: number, y: number}[]) => [...acc, ...next], [])
  }

  const handleClick = (evt: any) => {
    dispatch(GameActionCreator.searchGame())
    console.log(mapShipsToPositionArray())
    client.send(JSON.stringify({command: 'create', payload: mapShipsToPositionArray() }))
  }

  if(playing || searching) return null
  return (
    <button className={'PlayButton'} onClick={handleClick}> Play </button>
  )
} 