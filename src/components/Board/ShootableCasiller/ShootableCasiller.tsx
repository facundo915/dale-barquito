import React from 'react'
import { ReducerContext } from '../../../reducer/appReducer/ReducerContext'
import { client } from '../../../constants/Constants'

interface Props {
  x: number
  y: number
}

export const ShootableCasiller: React.FC<Props> = ({x, y}) => {

  const [state, dispatch] = React.useContext(ReducerContext)
  const { boardState, gameState } = state
  const { isShot, hit } = boardState[`${x}${y}`] || {isShot: false, isShip: false}
  const { myTurn } = gameState

  const shoot = () => {
    if (myTurn && !isShot) {
      client.send(JSON.stringify({ command: "shoot", payload: { x, y } }))
    }
  }

  return (
    <div className={`casiller ${isShot? `shot ${ hit ? 'hit' : ''}` : ''} ${(myTurn) ? '' : ' disabled'}`}
         onClick={shoot}
    />
  )
} 