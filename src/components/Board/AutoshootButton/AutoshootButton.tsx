import React from 'react'
import './AutoshootButton.scss'
import { client } from '../../../constants/Constants'
import { ReducerContext } from '../../../reducer/appReducer/ReducerContext'

interface Props {

}

export const AutoshootButton: React.FC<Props> = () => {
  const [state, dispatch] = React.useContext(ReducerContext)
  const { gameState } = state
  const { playing, myTurn } = gameState


  const handleAutoshoot = () => {
    client.send(JSON.stringify({ command: "shoot" }))
  }

  if(!playing || !myTurn) return null

  return (
    <button className={'AutoshootButton'} onClick={handleAutoshoot}> Auto shoot </button>
  )
} 