import React, { useEffect, useRef } from 'react'
import './GamePage.scss'
import { Board } from '../Board/Board'
import { client } from '../../constants/Constants'
import { ReducerContext } from '../../reducer/appReducer/ReducerContext'
import { PlayButton } from '../Board/PlayButton/PlayButton'
import { WaitingTitle } from '../waitingTitle/WaitingTitle'
import { EventReducer } from '../../reducer/eventReducer/EventReducer'
import { FinishTitle } from '../FinishTitle/FinishTitle'
import { Timer } from '../Board/Timer/Timer'
import { AutoshootButton } from '../Board/AutoshootButton/AutoshootButton'

interface Props {

}


export const GamePage: React.FC<Props> = () => {

  const [state, dispatch] = React.useContext(ReducerContext)
  let eventReducer = useRef(EventReducer(dispatch)(state))

  React.useEffect(() => {
    eventReducer.current = EventReducer(dispatch)(state)
  },[state])

  useEffect(() => {
    client.onopen = () => {
      console.log('WebSocket Client Connected');
      client.send(JSON.stringify({ command : "reconnect" }))
    };
    client.onmessage = (message) => {
      console.log(message)
      eventReducer.current(JSON.parse(message.data as string))
    };
  },[])

  return (
    <div className={'GamePage'}>
      <h1 className={'main-title'}>Dale Barquito</h1>

      <FinishTitle/>

      <div className={'boards-container'}>
        <Board rows={10} columns={10} />
        <WaitingTitle/>
        <Board rows={10} columns={10} shootable />
      </div>
      <div>
        <PlayButton/>
        <AutoshootButton/>
       <Timer/>
      </div>
    </div>
  )
}




