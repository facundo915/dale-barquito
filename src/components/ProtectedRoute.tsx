import React from 'react'
import { Redirect, Route, RouteProps } from 'react-router-dom'

interface Props {
    component: React.ComponentType
}

export const ProtectedRoute: React.FC<Props & RouteProps> = ({component: Component, ...rest}) => {

  const isLoggedIn = (): boolean => !!window.sessionStorage.getItem("token")

  return (
      <Route {...rest} render={ props => (
         isLoggedIn() ?
          <Component {...props} />
          : <Redirect to="/login" />
      )} />
    );
};

