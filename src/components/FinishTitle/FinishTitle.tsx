import React from 'react'
import { ReducerContext } from '../../reducer/appReducer/ReducerContext'
import './FinishTitle.scss'
import YouTube from '@u-wave/react-youtube';
import { GameActionCreator } from '../../reducer/gameReducer/gameActions'
import { ShipsActionCreator } from '../../reducer/shipsReducer/shipsActions'
import { BoardReducerActionCreator } from '../../reducer/boardReducer/BoardActions'


interface Props {

}

export const FinishTitle: React.FC<Props> = () => {
  const [ state, dispatch ] = React.useContext(ReducerContext)
  const { gameState } = state
  const { win, lost, enemyDisconected } = gameState

  const handleReplay = () => {
    dispatch(GameActionCreator.clear())
    dispatch(ShipsActionCreator.clear())
    dispatch(BoardReducerActionCreator.clear())
  }

  if (win) return (
    <dialog className={'dialog'}>
      <div>
        {enemyDisconected && <h1> El enemigo se desconecto, Ganaste! </h1>}
        {!enemyDisconected && <h1> WIN! </h1>}
        <YouTube
          className={'hidden'}
          volume={1}
          video="-ejya93VLj8"
          autoplay
          startSeconds={972}
          width={560}
          height={315}
          endSeconds={976}
        />

        <button onClick={handleReplay}> Volver a jugar </button>
      </div>
    </dialog>
  )

  if (lost) return (
    <dialog className={'dialog'}>
      <div>
        <h1> PERDISTE! </h1>
        <YouTube
          className={'hidden'}
          volume={1}
          video="FLqvKFoG2Fc"
          autoplay
          startSeconds={39}
          width={560}
          height={315}
        />

        <button onClick={handleReplay}> Volver a jugar </button>
      </div>
    </dialog>
  )


  return null
}