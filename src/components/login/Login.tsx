import React from 'react'
import GoogleLogin, { GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login'
import './Login.scss'
import YouTube from '@u-wave/react-youtube'

interface Props {}

export const Login: React.FC<Props> = () => {
  const handleSuccess = (data: GoogleLoginResponse | GoogleLoginResponseOffline) => {
    data = data as GoogleLoginResponse
    const token = data.tokenId
    window.sessionStorage.setItem('token',token)
    window.location.href = 'play'
  }

    return(
      <div className={'login-container'}>
        <h1 className={'main-title'}> Dale barquito </h1>
        <div className={'login-button-container'}>
          <h2 className={'title'}> Juga con tu cuenta de google </h2>
          <GoogleLogin
              clientId="519899469178-7g0u0f0vv1kquvrp9bb50hqe2t7ra52u.apps.googleusercontent.com"
              buttonText=" Jugar "
              onSuccess={ handleSuccess }

              onFailure={ (...data) => {
                console.log(data)
              }}
              cookiePolicy={'single_host_origin'}
          />
        </div>
        <YouTube
          className={'daleBarquito'}
          volume={1}
          video="b886d5QCeac"
          autoplay
          startSeconds={10}
          endSeconds={30}
          width={600}
          height={360}
        />
      </div>
        )
}