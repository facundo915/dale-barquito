import React from 'react'
import { ReducerContext } from '../../reducer/appReducer/ReducerContext'

interface Props {

}

export const TurnTitle: React.FC<Props> = () => {

  const [state] = React.useContext(ReducerContext)
  const { gameState } = state
  const { myTurn, playing, win, lost } = gameState

  if(!playing || win || lost) return null
  if(myTurn) return <h3> Tu turno de tirar </h3>
  return <h3> El enemigo esta jugando... 😟 </h3>
} 