import React from 'react'
import { ReducerContext } from '../../reducer/appReducer/ReducerContext'
import './WaitingTitle.scss'

interface Props {

}

export const WaitingTitle: React.FC<Props> = () => {

  const [ state ] = React.useContext(ReducerContext)
  const { gameState } = state
  const { searching } = gameState

  if(!searching) return null

  return (
    <h2 className={'WaitingTitle'}>
      Looking for oponent...
    </h2>
  )
} 