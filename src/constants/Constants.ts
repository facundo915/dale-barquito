import { w3cwebsocket as W3CWebSocket } from 'websocket'
import { ShipModel } from '../models/ShipModel'

export const client = new W3CWebSocket(`wss://fa2yni6d4l.execute-api.us-east-1.amazonaws.com/dev?token=${window.sessionStorage.getItem('token')}`);


export const initialShips: ShipModel[] = [
  { id: 1, length: 4, orientation: 'column' , position: { x: 1, y: 1 }, hit: []},
  { id: 2, length: 3, orientation: 'column' , position: { x: 5, y: 1 }, hit: []},
  { id: 3, length: 3, orientation: 'row' , position: { x: 1, y: 6 }, hit: []},
  { id: 4, length: 2, orientation: 'row' , position: { x: 5, y: 8}, hit: []},
  { id: 5, length: 2, orientation: 'column' , position: { x: 7, y: 0 }, hit: []},
  { id: 6, length: 2, orientation: 'column' , position: { x: 8, y: 8 }, hit: []},
  { id: 7, length: 1, orientation: 'row' , position: { x: 5, y: 5 }, hit: []},
  { id: 8, length: 1, orientation: 'column' , position: { x: 7, y: 5 }, hit: []},
  { id: 9, length: 1, orientation: 'column' , position: { x: 0, y: 9 }, hit: []},
  { id: 10, length: 1, orientation: 'column', position: { x: 2, y: 8 }, hit: []},
]