import React from 'react'
import './App.css'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Login } from './components/login/Login'
import { GamePage } from './components/gamePage/GamePage'
import { ProtectedRoute } from './components/ProtectedRoute'
import { ReducerContext } from './reducer/appReducer/ReducerContext'
import { AppReducer } from './reducer/appReducer/AppReducer'
import { appReducerInitialState } from './reducer/appReducer/ReducerState'



function App() {
  const Provider = ReducerContext.Provider
  const appReducer = React.useReducer(
    AppReducer,
    appReducerInitialState
  )
  return (
    <div className="App">
      <Provider value={appReducer}>
        <Router>
          <Switch>
            <ProtectedRoute path={'/play'} component={GamePage}/>
            <Route path={'/'} component={Login}/>
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
