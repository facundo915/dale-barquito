import { ReducerState } from './ReducerState'
import { ReducerAction } from './ReducerActions'
import React from 'react'

export const AppReducer: React.Reducer<ReducerState, ReducerAction> = (
  state: ReducerState,
  action: ReducerAction
) => ({...state, [action.stateKey]: action.handler(state[action.stateKey], action)})


