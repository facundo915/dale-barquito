import { DragAndDropReducerInitialState, DragAndDropState } from '../dragAndDropReducer/DragAndDropState'
import { BoardReducerState } from '../boardReducer/BoardState'
import { gameReducerInitialState, GameState } from '../gameReducer/gameState'
import { shipsReducerInitialState, ShipsState } from '../shipsReducer/shipsState'

export interface ReducerState {
  boardState: BoardReducerState
  dragAndDrop: DragAndDropState
  gameState: GameState
  ships: ShipsState
}

export const appReducerInitialState: ReducerState = {
  boardState: {},
  dragAndDrop: DragAndDropReducerInitialState,
  gameState: gameReducerInitialState,
  ships: shipsReducerInitialState
}

export type ReducerStateKeys = keyof ReducerState