import React from 'react'
import { ReducerState } from './ReducerState'
import { ReducerAction } from './ReducerActions'

//@ts-ignore
export const ReducerContext = React.createContext<[ReducerState, React.Dispatch<ReducerAction>]>(undefined)