import { ReducerState, ReducerStateKeys } from './ReducerState'

export type ReducerAction<Payload = undefined, StateKey extends ReducerStateKeys = ReducerStateKeys> = {
  payload: Payload
  stateKey: StateKey
  handler: ReducerHandler<Payload, StateKey>
}

export type ReducerHandler <Payload, StateKey extends ReducerStateKeys> = (
  state: ReducerState[StateKey],
  action: ReducerAction<Payload, StateKey>
) => ReducerState[StateKey];