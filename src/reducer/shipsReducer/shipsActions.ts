import { ReducerAction } from '../appReducer/ReducerActions'
import { ChangeOrientationHandler, clearHandler, HitShipHandler, MoveShipHandler } from './shipsHandlers'
import { ReducerStateKeys } from '../appReducer/ReducerState'

const ShipsStateKey: ReducerStateKeys = 'ships'

export type SimpleShipActionPayload = { id: number }
export type SimpleShipAction = ReducerAction<SimpleShipActionPayload, typeof ShipsStateKey>

export type MoveShipActionPayload = { id: number, x: number, y: number }
export type MoveShipAction = ReducerAction<MoveShipActionPayload, typeof ShipsStateKey>

export type HitShipShipPayload = { x: number, y: number }
export type HitShipShipAction = ReducerAction<HitShipShipPayload, typeof ShipsStateKey>

export type ShipEmptyAction = ReducerAction<undefined, typeof ShipsStateKey>

export const ShipsActionCreator = {
  moveShip: ( payload: MoveShipActionPayload ): MoveShipAction =>
    ({
      handler: MoveShipHandler,
      stateKey: ShipsStateKey,
      payload
    }),

  changeOrientation: ( payload: SimpleShipActionPayload ): SimpleShipAction =>
    ({
      handler: ChangeOrientationHandler,
      stateKey: ShipsStateKey,
      payload
    }),

  hit: ( payload: HitShipShipPayload ): HitShipShipAction =>
    ({
      handler: HitShipHandler,
      stateKey: ShipsStateKey,
      payload
    }),

  clear: (): ShipEmptyAction => ({
    handler: clearHandler,
    stateKey: ShipsStateKey,
    payload: undefined
  })
}