import { initialShips } from '../../constants/Constants'
import { ShipModel } from '../../models/ShipModel'

export type ShipsState = ShipModel[]

export const shipsReducerInitialState: ShipsState = initialShips