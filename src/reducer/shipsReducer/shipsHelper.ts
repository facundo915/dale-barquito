import { StartDragPayload } from '../dragAndDropReducer/DragAndDropActions'
import { ShipsState } from './shipsState'
import { ShipModel } from '../../models/ShipModel'

export const validateShipPosition = (ships: ShipsState) => (shipInfo: StartDragPayload) => (position: {x: number, y: number}): boolean => {
  const { shipLength, orientation, id } = shipInfo
  const column = orientation === 'column'
  const allOtherShips = ships.filter( ship => ship.id !== id)

  if (!validateShipBoundaries(position)(shipLength)(column)) return false

  const newPositions = mapShipToNearbyPositions({id, position, length: shipLength, orientation, hit: []})
  const allOtherShipsPositions = allOtherShips.map(mapShipToActualPositions).reduce((acc , next) => [...acc, ...next], [])

  return !allOtherShipsPositions.some( shipPosition => newPositions.some( newPosition => shipPosition.x === newPosition.x && shipPosition.y === newPosition.y))
}

const validateShipBoundaries = ({x , y}: {x: number, y: number}) => (shipLength: number) => (column: boolean) => {
  if (!column) return x >= 0 && x + shipLength <= 10
  else return y >= 0 && y + shipLength <= 10
}

export const mapShipToActualPositions = (ship: ShipModel) => {
  const { orientation, length, position } = ship
  const { x, y } = position
  const column = orientation === 'column'

  let newPositions = []

  if (!column)
    newPositions = Array.from(Array(length).keys())
      .map( i => ({x: x + i, y}))
  else
    newPositions = Array.from(Array(length).keys())
      .map( i => ({x, y: y + i}))

  return newPositions
}

const mapShipToNearbyPositions = (ship: ShipModel) => {
  const { orientation, length, position } = ship
  const { x, y } = position
  const column = orientation === 'column'

  let newPositions = []

  if (!column)
    newPositions = Array.from(Array(length + 2).keys())
      .map( i => i - 1)
      .map( i => ({x: x + i, y}))
      .reduce( (acc: {x: number, y: number}[] , next) => [...acc, next, {x: next.x, y: next.y + 1}, {x: next.x, y: next.y - 1}],[])

  else
    newPositions = Array.from(Array(length + 2).keys())
      .map( i => i - 1)
      .map( i => ({x, y: y + i}))
      .reduce( (acc: {x: number, y: number}[] , next) => [...acc, next, {x: next.x + 1, y: next.y}, {x: next.x - 1, y: next.y}],[])

  return newPositions
}
