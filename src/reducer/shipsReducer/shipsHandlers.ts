import { ShipsState } from './shipsState'
import { HitShipShipAction, MoveShipAction, SimpleShipAction } from './shipsActions'
import { mapShipToActualPositions } from './shipsHelper'

export const MoveShipHandler = ( state: ShipsState, { payload }: MoveShipAction): ShipsState => {
  const { x , y , id} = payload
  return state.map( ship => {
    if (ship.id === id) return {...ship, position: { x , y }}
    else return ship
  })
}

export const ChangeOrientationHandler = ( state: ShipsState, { payload }: SimpleShipAction): ShipsState => {
  return state.map( ship => {
    if (ship.id === payload.id) return {...ship, orientation: ship.orientation === "row" ? "column": "row"}
    return ship
  })
}

export const HitShipHandler = ( state: ShipsState, { payload }: HitShipShipAction): ShipsState => {
  const { x , y } = payload
  const mappedShips = state.map(mapShipToActualPositions)
  const position = mappedShips.find(positionList => positionList.some(position => position.x === x && position.y === y))
  const index = mappedShips.indexOf(position!)
  const { id, orientation, position: shipPosition } = state[index]
  const hitSpot =  orientation === "column" ? y - shipPosition.y : x - shipPosition.x

  return state.map( ship => {
    if (ship.id === id) return {...ship, hit: [...ship.hit, hitSpot]}
    return ship
  })
}

export const clearHandler = ( state: ShipsState ): ShipsState => {
  return state.map(ship => ({...ship, hit: []}))
}