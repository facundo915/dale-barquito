export type MoveEvent = { event: "MOVE", sink: undefined }
export type WaitEvent = { event: "WAIT" } | { event: "WAIT", shot: { x: number, y: number } }
export type HitEvent =  { event: "HIT", sink: boolean, shot: { x: number, y: number } }
export type MissEvent =  { event: "MISS", shot: { x: number, y: number } }
export type WinEvent = { event: "WIN" }
export type LooseEvent = { event: "ENEMY_WIN" }
export type EnemyShotEvent = { event: "ENEMY_SHOT", shot: { x: number, y: number } }
export type EnemyDisconectedEvent = { event: "ENEMY_DISCONNECTED" }

export type EventActions = MoveEvent | WaitEvent | HitEvent | MissEvent | WinEvent | LooseEvent | EnemyShotEvent | EnemyDisconectedEvent