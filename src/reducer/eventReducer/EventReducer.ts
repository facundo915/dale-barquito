import { EventActions } from './EventActions'
import { GameActionCreator } from '../gameReducer/gameActions'
import { BoardReducerActionCreator } from '../boardReducer/BoardActions'
import { ReducerState } from '../appReducer/ReducerState'
import { mapShipToActualPositions } from '../shipsReducer/shipsHelper'
import { ShipsActionCreator } from '../shipsReducer/shipsActions'

export const EventReducer = (dispatch: React.Dispatch<any>) => (state: ReducerState) => (message: EventActions): void => {
  switch (message.event) {
    case "MOVE": {
      dispatch(GameActionCreator.foundGame())
      dispatch(GameActionCreator.startTimer())
      dispatch(GameActionCreator.setTurn({myTurn: true}))
      break
    }
    case 'WAIT': {
      dispatch(GameActionCreator.foundGame())
      dispatch(GameActionCreator.pauseTimer())
      dispatch(GameActionCreator.resetTimer())
      dispatch(GameActionCreator.setTurn({myTurn: false}))
      break
    }
    case 'HIT': {
      const { shot, sink } = message
      const { x, y } = shot
      dispatch(BoardReducerActionCreator.hit({x, y, hit: true}))
      sink && dispatch(BoardReducerActionCreator.sink({x, y, hit: true}))
      dispatch(GameActionCreator.resetTimer())
      break
    }
    case 'MISS': {
      const { shot } = message
      const { x, y } = shot
      dispatch(BoardReducerActionCreator.hit({x, y, hit: false}))
      dispatch(GameActionCreator.setTurn({myTurn: false}))
      dispatch(GameActionCreator.pauseTimer())
      dispatch(GameActionCreator.resetTimer())
      break
    }
    case 'WIN': {
      dispatch(GameActionCreator.pauseTimer())
      dispatch(GameActionCreator.resetTimer())
      dispatch(GameActionCreator.setTurn({myTurn: false}))
      dispatch(GameActionCreator.win())
      break
    }
    case 'ENEMY_WIN': {
      dispatch(GameActionCreator.pauseTimer())
      dispatch(GameActionCreator.resetTimer())
      dispatch(GameActionCreator.setTurn({myTurn: false}))
      dispatch(GameActionCreator.loose())
      break
    }
    case 'ENEMY_SHOT': {
      const { shot } = message
      const { x, y } = shot
      debugger
      const hit =
        state.ships
          .map(mapShipToActualPositions)
          .reduce( (acc, next) => [...acc, ...next] ,[])
          .some( position => position.x === x && position.y === y)

      if(hit) dispatch(ShipsActionCreator.hit({ x ,y }))
      else dispatch(BoardReducerActionCreator.enemyHit({x , y, hit: false}))
      break
    }
    case 'ENEMY_DISCONNECTED': {
      dispatch(GameActionCreator.pauseTimer())
      dispatch(GameActionCreator.enemyDisconected())
      dispatch(GameActionCreator.resetTimer())
      dispatch(GameActionCreator.setTurn({myTurn: false}))
      dispatch(GameActionCreator.win())
      break
    }
    default: {}
  }
}