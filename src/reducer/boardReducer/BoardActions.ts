import { ReducerAction } from '../appReducer/ReducerActions'
import {
  clearHandler,
  enemyHitHandler,
  hitHandler,
  InitializeCasillerHandler,
  PlaceShipHandler,
  shootLocationHandler, sinkHandler,
} from './BoardHandlers'
import { ReducerStateKeys } from '../appReducer/ReducerState'
import { StartDragPayload } from '../dragAndDropReducer/DragAndDropActions'

const BoardStateKey: ReducerStateKeys = 'boardState'
type BoardStateAction<Payload> = ReducerAction<Payload, typeof BoardStateKey>


type InitializeCasillerPayload = { x: number, y: number }
export type InitializeCasillerAction = BoardStateAction<InitializeCasillerPayload>

export type PlaceShipPayload = StartDragPayload & { x: number, y: number }
export type PlaceShipAction = BoardStateAction<PlaceShipPayload>

export type ShootPayload = { x: number, y: number }
export type ShootAction = BoardStateAction<ShootPayload>

export type HitPayload = { x: number, y: number, hit: boolean }
export type HitAction = BoardStateAction<HitPayload>

export type ClearAction = BoardStateAction<undefined>


export const BoardReducerActionCreator = {
  initializeCasiller: (data: InitializeCasillerPayload): InitializeCasillerAction =>
    ({
      handler: InitializeCasillerHandler,
      payload: data,
      stateKey: BoardStateKey
    }),

  placeShip: (draggedShip: PlaceShipPayload): PlaceShipAction =>
    ({
      handler: PlaceShipHandler,
      payload: draggedShip,
      stateKey: BoardStateKey
    }),

  shoot: (shootLocationPayload: ShootPayload): ShootAction =>
    ({
      handler: shootLocationHandler,
      payload: shootLocationPayload,
      stateKey: BoardStateKey
    }),

  hit: (shootLocationPayload: HitPayload): HitAction =>
    ({
      handler: hitHandler,
      payload: shootLocationPayload,
      stateKey: BoardStateKey
    }),

  enemyHit: (shootLocationPayload: HitPayload): HitAction =>
    ({
      handler: enemyHitHandler,
      payload: shootLocationPayload,
      stateKey: BoardStateKey
    }),

  sink: (shootLocationPayload: HitPayload): HitAction =>
    ({
      handler: sinkHandler,
      payload: shootLocationPayload,
      stateKey: BoardStateKey
    }),

  clear: (): ClearAction => ({
    handler: clearHandler,
    stateKey: BoardStateKey,
    payload: undefined
  })
}