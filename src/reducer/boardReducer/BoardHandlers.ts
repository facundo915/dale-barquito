import { HitAction, InitializeCasillerAction, PlaceShipAction, ShootAction } from './BoardActions'
import { BoardReducerState } from './BoardState'
import {
  hasNearbyShips,
  placeShip,
  reduceListToObject,
  shipBoundarysAreOk,
  shootAllNerbyCasillers,
} from './BoardHandlersHelper'

export const InitializeCasillerHandler = (state: BoardReducerState, action: InitializeCasillerAction ): BoardReducerState => {
  return { ...state,
    [action.payload.x.toString() + action.payload.y.toString()]: {
      x: action.payload.x,
      y: action.payload.y,
      isShip: false,
      isShot: false,
      hit: false,
      enemyHit: false,
      enemyShot: false
  }}
}

export const PlaceShipHandler = (state: BoardReducerState, { payload }: PlaceShipAction ): BoardReducerState => {
  const {x, y, draggedFrom, orientation, shipLength} = payload
  switch(orientation) {
    case 'column': {
      const y0 = y - draggedFrom
      if(shipBoundarysAreOk(y0, shipLength) && !hasNearbyShips(state, y0 - 1, y0 + shipLength, x - 1, x + 1)){
        return placeShip(state, shipLength, i => state[`${x}${y0 + i}`])
      }
      return {...state}
    }

    case 'row': {
      const x0 = x - draggedFrom
      if(shipBoundarysAreOk(x0, shipLength) && !hasNearbyShips(state, y - 1, y + 1, x0 - 1, x0 + shipLength)){
        return placeShip(state, shipLength, i => state[`${x0 + i}${y}`])
      }
      return {...state}
    }
    default: return {...state}
  }
}

export const shootLocationHandler = (state: BoardReducerState, { payload }: ShootAction ): BoardReducerState => {
  const {x, y} = payload
  const accesor = `${x}${y}`
  return {...state, [accesor]: {...state[accesor], isShot: true}}
}

export const hitHandler = (state: BoardReducerState, { payload }: HitAction ): BoardReducerState => {
  const {x, y, hit} = payload
  const accesor = `${x}${y}`
  return {...state, [accesor]: { ...state[accesor], hit, isShot: true }}
}

export const enemyHitHandler = (state: BoardReducerState, { payload }: HitAction ): BoardReducerState => {
  const {x, y, hit} = payload
  const accesor = `${x}${y}`
  return {...state, [accesor]: { ...state[accesor], enemyHit: hit, enemyShot: true }}
}

export const sinkHandler = (state: BoardReducerState, { payload }: HitAction ): BoardReducerState => {
  const {x, y} = payload
  return {...state, ...shootAllNerbyCasillers(state)({x,y}) }
}

export const clearHandler = (state: BoardReducerState): BoardReducerState => {
  return Object.keys(state)
    .map(key => state[key])
    .map( position => ({...position, hit: false, isShot: false, enemyHit: false, enemyShot: false, disabled: false}))
    .reduce( (acc, next) => ({...acc, [`${next.x}${next.y}`]: next}), {})
}