import { BoardReducerState } from './BoardState'

export const hasNearbyShips = (state: BoardReducerState, bottom: number, top: number, left: number, right: number) => {
  for (let x = left; x <= right; x++) {
    if ( state[`${x}${top}`]?.isShip || state[`${x}${bottom}`]?.isShip ) return true
  }
  for (let y = bottom; y <= top; y++) {
    if ( state[`${left}${y}`]?.isShip || state[`${right}${y}`]?.isShip ) return true
  }
  return false
}

export const shipBoundarysAreOk = (z0: number, shipLength: number) => {
  return z0 >= 0 && (z0 + shipLength) <= 10
}

export const placeShip = (state: BoardReducerState, shipLength: number, mapper: (i: number) => BoardReducerState[0]) => (
  {
    ...state,
    ...Array.from(Array(shipLength).keys())
      .map( mapper )
      .reduce( (obj, next) => ({...obj, [`${next.x}${next.y}`]: {...next, isShip: true}}), {})
  }
)

export const validateColumnShipPlacement = (state: BoardReducerState, shipLength: number, x: number, y0: number,) =>
  shipBoundarysAreOk(y0, shipLength) && !hasNearbyShips(state, y0 - 1, y0 + shipLength, x - 1, x + 1)

export const validateRowShipPlacement = (state: BoardReducerState, shipLength: number, x0: number, y: number,) =>
  shipBoundarysAreOk(x0, shipLength) && !hasNearbyShips(state, y - 1, y + 1, x0 - 1, x0 + shipLength)

export const reduceListToObject = <T> (arr: T[]) => (mapper: (elem: T) => object ) =>
  arr.reduce((acc, next) => ({...acc, ...mapper(next)}), {})

export const shootAllNerbyCasillers = (state: BoardReducerState) => (elem: {x: number, y: number}): BoardReducerState => {
  return getShipFromCasiller(state, elem, [state[`${elem.x}${elem.y}`]])
    .map( shipPart =>
      Object.keys(state).map(key => state[key]).filter( casiller => isAdyacent(shipPart.x)(casiller.x) && isAdyacent(shipPart.y)(casiller.y))
    ).reduce( (acc, next) => [...acc, ...next], [])
    .map( casiller => ({...casiller, isShot: true}))
    .reduce( (newState, casiller) => ({...newState, [`${casiller.x}${casiller.y}`]: casiller}), {})

}
const getShipFromCasiller = (state: BoardReducerState, elem: {x: number, y: number}, currentShip: BoardReducerState[0][]): BoardReducerState[0][] => {
  const adyacentShipParts = getAdyacentShipParts(state, elem)
  return adyacentShipParts.reduce( (shipParts: BoardReducerState[0][], shipPart) => {
    if (!currentShip.some( part => part.x === shipPart.x && part.y === shipPart.y))
      return [...adyacentShipParts, shipPart, ...getShipFromCasiller(state, shipPart, [...adyacentShipParts, ...currentShip])]
    return [...shipParts, shipPart]
  }, [])
}

const getAdyacentShipParts = (state: BoardReducerState, {x, y}: {x: number, y: number}): BoardReducerState[0][] =>
  Object.keys(state)
    .map( key => state[key])
    .filter( elem => elem.hit && isAdyacent(x)(elem.x) && isAdyacent(y)(elem.y))

const isAdyacent = (source: number) => (guess: number) => guess === source - 1 || guess === source + 1 || guess === source