export type BoardReducerState = {
  [str: string]: {
    x: number
    y: number
    isShot: boolean
    isShip: boolean
    hit: boolean
    disabled?: boolean
    enemyShot: boolean
    enemyHit: boolean
  }
}