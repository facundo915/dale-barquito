import { Orientation } from '../../components/Board/Ship/Ship'
import { ReducerAction } from '../appReducer/ReducerActions'
import { StartDragHandler } from './DragAndDropHandlers'
import { ReducerStateKeys } from '../appReducer/ReducerState'

const DragAndDropStateKey: ReducerStateKeys = 'dragAndDrop'

export interface StartDragPayload {
  shipLength: number,
  draggedFrom: number,
  orientation: Orientation,
  id: number
}
export type StartDragAction = ReducerAction<StartDragPayload, typeof DragAndDropStateKey>


export const DragAndDropActionCreator = {
  startDrag: (payload: StartDragPayload): StartDragAction =>
    ({
      payload,
      stateKey: DragAndDropStateKey,
      handler: StartDragHandler
    }),
}