import { DragAndDropState } from './DragAndDropState'
import { StartDragAction } from './DragAndDropActions'

export const StartDragHandler = (state: DragAndDropState , action: StartDragAction ): DragAndDropState => {
  return { ...state, currentDrag: action.payload }
}
