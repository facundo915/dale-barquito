import { StartDragPayload } from './DragAndDropActions'

export interface DragAndDropState {
  currentDrag: StartDragPayload | undefined
  dropped: boolean
}

export const DragAndDropReducerInitialState: DragAndDropState = {
  currentDrag: undefined,
  dropped: false
}