import { ReducerAction } from '../appReducer/ReducerActions'
import {
  SearchGameHandler,
  foundGameHandler,
  turnHandler,
  winHandler,
  looseHandler,
  clearHandler,
  startTimerHandler,
  pauseTimerHandler,
  resetTimerHandler,
  addToTimerHandler,
  enemyDisconectedHandler,
} from './gameHandlers'
import { ReducerStateKeys } from '../appReducer/ReducerState'

const GameStateKey: ReducerStateKeys = 'gameState'

export type GameEmptyAction = ReducerAction<undefined, typeof GameStateKey>

export type SetTurnPayload = { myTurn: boolean }
export type SetTurnAction = ReducerAction<SetTurnPayload, typeof GameStateKey>


export const GameActionCreator = {
  searchGame: (): GameEmptyAction =>
    ({
      handler: SearchGameHandler,
      stateKey: GameStateKey,
      payload: undefined
    }),

  foundGame: (): GameEmptyAction =>
    ({
      handler: foundGameHandler,
      stateKey: GameStateKey,
      payload: undefined
    }),

  setTurn: ( payload: SetTurnPayload ): SetTurnAction =>
    ({
      handler: turnHandler,
      stateKey: GameStateKey,
      payload
    }),

  win: (): GameEmptyAction =>
    ({
      handler: winHandler,
      stateKey: GameStateKey,
      payload: undefined
    }),

  loose: (): GameEmptyAction =>
    ({
      handler: looseHandler,
      stateKey: GameStateKey,
      payload: undefined
    }),

  clear: (): GameEmptyAction => ({
    handler: clearHandler,
    stateKey: GameStateKey,
    payload: undefined
  }),

  startTimer: (): GameEmptyAction => ({
    handler: startTimerHandler,
    stateKey: GameStateKey,
    payload: undefined
  }),

  pauseTimer: (): GameEmptyAction => ({
    handler: pauseTimerHandler,
    stateKey: GameStateKey,
    payload: undefined
  }),

  resetTimer: (): GameEmptyAction => ({
    handler: resetTimerHandler,
    stateKey: GameStateKey,
    payload: undefined
  }),

  takeFromTimer: (): GameEmptyAction => ({
    handler: addToTimerHandler,
    stateKey: GameStateKey,
    payload: undefined
  }),

  enemyDisconected: (): GameEmptyAction => ({
    handler: enemyDisconectedHandler,
    stateKey: GameStateKey,
    payload: undefined
  })
}