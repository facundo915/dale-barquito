import { gameReducerInitialState, GameState } from './gameState'
import { SetTurnAction } from './gameActions'

export const SearchGameHandler = (state: GameState ): GameState => {
  return { ...state, searching: true }
}

export const foundGameHandler = (state: GameState ): GameState => {
  return { ...state, searching: false, playing: true }
}

export const turnHandler = (state: GameState, {payload}: SetTurnAction ): GameState => {
  return {...state, myTurn: payload.myTurn}
}

export const winHandler = (state: GameState ): GameState => {
  return {...state, win: true}
}

export const looseHandler = (state: GameState ): GameState => {
  return {...state, lost: true}
}

export const clearHandler = ( ): GameState => {
  return gameReducerInitialState
}

export const startTimerHandler = (state: GameState ): GameState => {
  return {...state, isTimerActive: true }
}

export const pauseTimerHandler = (state: GameState ): GameState => {
  return {...state, isTimerActive: false }
}

export const resetTimerHandler = (state: GameState ): GameState => {
  return {...state, time: gameReducerInitialState.time }
}

export const addToTimerHandler = (state: GameState ): GameState => {
  return {...state, time: state.time - 1 }
}

export const enemyDisconectedHandler = (state: GameState ): GameState => {
  return {...state, enemyDisconected: true }
}