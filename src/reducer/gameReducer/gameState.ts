export interface GameState {
  playing: boolean
  searching: boolean
  myTurn: boolean
  win: boolean
  lost: boolean
  time: number
  isTimerActive: boolean
  enemyDisconected: boolean
}

export const gameReducerInitialState: GameState = {
  playing: false,
  searching: false,
  myTurn: false,
  win: false,
  lost: false,
  time: 15,
  isTimerActive: false,
  enemyDisconected: false
}